CREATE TABLE students (
id BIGINT  PRIMARY KEY,
lastname VARCHAR,
firstname VARCHAR
 );

CREATE TABLE lessons (
id BIGINT  PRIMARY KEY,
name VARCHAR
 );

CREATE TABLE students_marks (
    id BIGINT  PRIMARY KEY,
    student_id BIGINT,
    lesson_id BIGINT,
    FOREIGN KEY (student_id) REFERENCES students(id),
    FOREIGN KEY (lesson_id) REFERENCES lessons(id),
    mark INT
);
CREATE TABLE student_lesson (
    student_id BIGINT,
    lesson_id BIGINT,
    FOREIGN KEY (student_id) REFERENCES students(id),
    FOREIGN KEY (lesson_id) REFERENCES lessons(id)
);