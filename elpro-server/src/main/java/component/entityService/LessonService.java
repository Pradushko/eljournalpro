package component.entityService;

import component.entity.Lesson;
import component.entityDAO.LessonDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
public class LessonService {
    @Autowired
    private LessonDAO lessonDAO;

    @Transactional
    public void addLesson(Lesson lesson) {
        lessonDAO.addLesson(lesson);
    }

    @Transactional
    public List<Lesson> listLesson() {
        return lessonDAO.listLesson();
    }

    @Transactional
    public void removeLesson(Long id) {
        lessonDAO.removeLesson(id);
    }

    @Transactional
    public Lesson getLesson(Long id){
        return lessonDAO.getLesson(id);
    }

    @Transactional
    public void edit(Lesson lesson){
        lessonDAO.edit(lesson);
    }
}
