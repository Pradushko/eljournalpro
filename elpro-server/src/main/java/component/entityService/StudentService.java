package component.entityService;

import component.entity.Student;
import component.entityDAO.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Admin on 23.09.2016.
 */


@Component
public class StudentService {

    @Autowired
    private StudentDAO studentDAO;

    @Transactional
    public void addStudent(Student student) {
        studentDAO.addStudent(student);
    }

    @Transactional
    public List<Student> listStudent() {
        return studentDAO.listStudent();
    }

    @Transactional
    public void removeStudent(Long id) {
        studentDAO.removeStudent(id);
    }

    @Transactional
    public Student getStudent(Long id){
        return studentDAO.getStudent(id);
    }

    @Transactional
    public void edit(Student student){
        studentDAO.edit(student);
    }


}
