package component.entity;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Admin on 18.08.16.
 */
@Component
@Entity
@Table(name="students")
public class Student implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="firstname")
    private String firstName;

    @Column(name="lastname")
    private String lastName;



    @ManyToMany
    @JoinTable(name="student_lesson",
            joinColumns={@JoinColumn(name="student_id")},
            inverseJoinColumns = {@JoinColumn(name="lesson_id")}
    )
     private Set<Lesson>lessons;

    @OneToMany(mappedBy="student")
    Set<StudentMark>studentsMarks;

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Student(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
