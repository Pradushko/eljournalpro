package component.entity;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Admin on 18.08.16.
 */
@Component
@Entity
@Table(name="lessons")
public class Lesson implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;


    @ManyToMany(mappedBy = "lessons")
    Set<Student>students;

    @OneToMany(mappedBy = "lesson")
    Set<StudentMark> studentsMarks;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
