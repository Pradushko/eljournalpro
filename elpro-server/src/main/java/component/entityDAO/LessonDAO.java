package component.entityDAO;

import component.entity.Lesson;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LessonDAO {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public void addLesson(Lesson lesson) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(lesson);
        session.getTransaction().commit();
        session.close();
    }

    public List<Lesson> listLesson() {
        return sessionFactory.openSession().createQuery("from Lesson ").list();
    }

    public void removeLesson(Long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Lesson lesson= (Lesson) session.get(Lesson.class,id);
        session.delete(lesson);
        session.getTransaction().commit();
        session.close();

    }

    public void edit(Lesson lesson){
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Lesson editLesson=(Lesson) session.get(Lesson.class,lesson.getId());
        editLesson.setName(lesson.getName());
        session.save(editLesson);
        session.getTransaction().commit();
        session.close();
    }
    public Lesson getLesson(Long id){
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Lesson lesson=(Lesson) session.get(Lesson.class,id);
        return lesson;
    }
}
