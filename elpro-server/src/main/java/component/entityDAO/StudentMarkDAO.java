package component.entityDAO;

import component.entity.StudentMark;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Admin on 16.09.2016.
 */
public class StudentMarkDAO  {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public void addStudentMark(StudentMark studentMark) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(studentMark);
        session.getTransaction().commit();
        session.close();
    }


    public List<StudentMark> listStudentMark(){
        return sessionFactory.getCurrentSession().createQuery("from StudentMark ").list();
    }

    public void removeStudentMark(StudentMark studentMark) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(studentMark);
        session.getTransaction().commit();
        session.close();
    }
}
