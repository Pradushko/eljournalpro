package component.entityDAO;

import component.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Admin on 13.09.2016.
 */

@Component
public class StudentDAO {


    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public void addStudent(Student student) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(student);
        session.getTransaction().commit();
        session.close();
    }

    public List<Student> listStudent() {
        return sessionFactory.openSession().createQuery("from Student ").list();
    }

    public void removeStudent(long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Student student= (Student) session.get(Student.class,id);
        session.delete(student);
        session.getTransaction().commit();
        session.close();
    }
    public Student getStudent(Long id){
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Student student=(Student) session.get(Student.class,id);
        return student;
    }

    public void edit(Student student){
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Student editStudent=(Student) session.get(Student.class,student.getId());
        editStudent.setFirstName(student.getFirstName());
        editStudent.setLastName(student.getLastName());
        session.save(editStudent);
        session.getTransaction().commit();
        session.close();
    }
}
