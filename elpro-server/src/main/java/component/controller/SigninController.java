package component.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Admin on 09.10.2016.
 */

@Controller
public class SigninController {
    @RequestMapping(value = "/signin", method = RequestMethod.GET)
        public String signin(){
            return "login";
        }
    @RequestMapping(value = "/signin-failure", method = RequestMethod.GET)
    public String signinFailure() {
        return "loginfail";
    }
    }

