package component.controller;


import component.entity.Lesson;
import component.entityService.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/lesson")
public class LessonController {
    @Autowired
    private LessonService lessonService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addLesson(@ModelAttribute(value = "lessonAttribute") Lesson lesson, Model model) {

        lessonService.addLesson(lesson);
        return "lesson/lessonadded";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddLesson(Model model) {
        // Create new Person and add to model
        // This is the formBackingOBject
        model.addAttribute("lessonAttribute", new Lesson());

        return "lesson/lessonadd";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listLesson(Model model) {
        List<Lesson> lessons = lessonService.listLesson();
        model.addAttribute("lessons", lessons);
        return "lesson/lessonlist";
    }


    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String removeLesson(@RequestParam(value = "id", required = true) long id, Model model) {
        lessonService.removeLesson(id);

        return "lesson/lessonremove";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String getEditLesson(@RequestParam(value = "id") Long id, Model model) {

        model.addAttribute("lessonAttribute", lessonService.getLesson(id));

        return "lesson/lessonedit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String getEditLesson(@ModelAttribute(value = "lessonAttribute") Lesson lesson, @RequestParam(value = "id") Long id,
                                 Model model) {
        lesson.setId(id);
        lessonService.edit(lesson);

        return "lesson/lessonedited";
    }


}
