package component.controller;

import component.entity.Student;
import component.entityService.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Admin on 23.09.2016.
 */

@Controller
@RequestMapping(value = "/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addStudent(@ModelAttribute(value = "studentAttribute") Student student, Model model) {

        studentService.addStudent(student);
        return "student/studentadded";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddStudent(Model model) {
        // Create new Person and add to model
        // This is the formBackingOBject
        model.addAttribute("studentAttribute", new Student());

        return "student/studentadd";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listStudent(Model model) {
        List<Student> students = studentService.listStudent();
        model.addAttribute("students", students);
        return "student/studentlist";
    }


    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String removeStudent(@RequestParam(value = "id", required = true) long id, Model model) {
        studentService.removeStudent(id);

        return "student/studentremove";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String getEditStudent(@RequestParam(value = "id") Long id, Model model) {

        model.addAttribute("studentAttribute", studentService.getStudent(id));

        return "student/studentedit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String getEditStudent(@ModelAttribute(value = "studentAttribute") Student student, @RequestParam(value = "id") Long id,
                                 Model model) {
        student.setId(id);
        studentService.edit(student);

        return "student/studentedited";
    }


}
